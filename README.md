Badge Tests
-----------

#### normal pipeline badge : 
- [![pipeline status1](https://git.ufz.de/palmb/badgetest/badges/master/pipeline.svg)](https://git.ufz.de/palmb/badgetest/-/pipelines/) 

##### normal pipeline badge on specific job (badge_build): 
- [![pipeline status2](https://git.ufz.de/palmb/badgetest/badges/master/pipeline.svg?job=badge_build)](https://git.ufz.de/palmb/badgetest/-/jobs/) 


#### hardcoded artifacts badge (this is what it should look like): 
- badge_build job: [![some1](https://git.ufz.de/palmb/badgetest/-/jobs/70919/artifacts/raw/badge.svg)](https://git.ufz.de/palmb/badgetest/-/jobs/69851/artifacts/browse?job=badge_build) 
- python38 job (failed, no overwrite): [![some1](https://git.ufz.de/palmb/badgetest/-/jobs/70920/artifacts/raw/badge.svg)](https://git.ufz.de/palmb/badgetest/-/jobs/69851/artifacts/browse?job=badge_use) 
- badge_use job: [![some1](https://git.ufz.de/palmb/badgetest/-/jobs/70921/artifacts/raw/badge.svg)](https://git.ufz.de/palmb/badgetest/-/jobs/69851/artifacts/browse?job=badge_use) 


#### dynamic artifacts badge : 
- badge_build job: [![some2](https://git.ufz.de/palmb/badgetest/-/jobs/artifacts/master/raw/badge.svg?job=badge_build)](https://git.ufz.de/palmb/badgetest/-/jobs/artifacts/master/browse?job=badge_build) 
- python38 job: [![some2](https://git.ufz.de/palmb/badgetest/-/jobs/artifacts/master/raw/badge.svg?job=python38)](https://git.ufz.de/palmb/badgetest/-/jobs/artifacts/master/browse?job=python38) 
- badge_use job: [![some2](https://git.ufz.de/palmb/badgetest/-/jobs/artifacts/master/raw/badge.svg?job=badge_use)](https://git.ufz.de/palmb/badgetest/-/jobs/artifacts/master/browse?job=badge_use)


